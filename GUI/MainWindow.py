from . import LearnView, VocabView, Settings
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GLib


# the main window of the application
# noinspection PyCallByClass,PyCallByClass
class Window(Gtk.Window):

    # noinspection PyCallByClass
    def __init__(self, app):
        Gtk.Window.__init__(self, title='AnkiPy')
        self.set_border_width(0.1)
        self.set_default_size(800, 400)
        self.connect("delete-event", Gtk.main_quit)
        self.set_name('MainWindow')
        self.app = app

        hb = Gtk.HeaderBar()
        hb.set_show_close_button(True)
        self.set_titlebar(hb)

        button = Gtk.ToggleButton(None, image=Gtk.Image(stock=Gtk.STOCK_PROPERTIES))
        button.set_active(False)
        hb.pack_end(button)
        button.connect('clicked', self.on_settings_clicked)

        window_container = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=1)
        self.add(window_container)

        views = Gtk.Stack()

        self.vocab_view = VocabView.Box(app)
        self.learn_view = LearnView.Box(app)
        views.add_titled(self.learn_view, 'learn', 'LEARN')
        views.add_titled(self.vocab_view, 'vocab', 'VOCAB')

        view_switcher = Gtk.StackSwitcher()
        view_switcher.set_stack(views)

        hb.set_custom_title(view_switcher)
        window_container.pack_start(views, True, True, 0)

        self.settings = Settings.Box(app)
        scrolled = Gtk.ScrolledWindow()
        scrolled.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        scrolled.add(self.settings)
        window_container.pack_end(scrolled, False, True, 0)

        load_css()
        self.show_all()
        self.settings.hide()
        app.gui = self
        Gtk.main()

    # sets the status text to inform user of whatever
    def set_status_text(self, msg):
        self.learn_view.task_label.set_markup('<big>' + msg + '</big>')

    # lock get new task key
    def lock_task(self):
        self.set_status_text('compile grammar')
        self.learn_view.task_button.set_sensitive(False)

    def on_ready(self):
        self.set_status_text('click >')
        self.learn_view.task_button.set_sensitive(True)
        self.vocab_view.export_btn.set_sensitive(True)

    def ready_callback(self):
        GLib.idle_add(self.on_ready)

    def on_settings_clicked(self, button):

        if button.get_active():
            self.settings.show_all()
        else:
            self.settings.hide()


# helper for setting css from file.
def load_css(css_path='./config/style.css'):
    # load themeing from css
    css = open(css_path, 'rb')
    css_data = css.read()
    css.close()

    style_provider = Gtk.CssProvider()
    style_provider.load_from_data(css_data)
    Gtk.StyleContext.add_provider_for_screen(
        Gdk.Screen.get_default(), style_provider,
        Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
    )

    return css_data
