import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from GF import Language


class Box(Gtk.ListBox):
    def __init__(self, app):
        Gtk.ListBox.__init__(self)
        self.set_selection_mode(Gtk.SelectionMode.NONE)
        self.app = app

        # init data
        if self.app.user_data.get('grammar') is None:
            self.app.user_data['grammar'] = {}
        if self.app.user_data.get('language') is None:
            self.app.user_data['language'] = ['Eng', 'Jpn']

        # language selection dropdown
        task_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=2)
        task_lbl = Gtk.Label('Task')
        self.task_drop = Gtk.ComboBoxText()
        task_box.pack_start(task_lbl, False, False, 0)
        task_box.pack_end(self.task_drop, False, False, 0)
        self.task_drop.connect('changed', self.on_set_lang, 1)

        ans_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=2)
        ans_lbl = Gtk.Label('Answer')
        self.ans_drop = Gtk.ComboBoxText()
        ans_box.pack_start(ans_lbl, False, False, 0)
        ans_box.pack_end(self.ans_drop, False, False, 0)
        self.ans_drop.connect('changed', self.on_set_lang, 0)

        for lng in Language.MAP:
            self.task_drop.append_text(lng)
            self.ans_drop.append_text(lng)

        self.add(task_box)
        self.add(ans_box)

        # the box where you select categories
        check_buttons = ('Nouns', 'Adjectives', 'Verbs', 'Present', 'Past', 'Future',
                         'Negative Present', 'Negative Past', 'Negative Future','Imperative', 'Question')

        self.check_list = {}
        for btn in check_buttons:
            self.check_list[btn] = Gtk.CheckButton(btn)
            self.check_list[btn].connect('toggled', self.update_grammar_selection, btn)
            self.add(self.check_list[btn])

        self.load_grammar_selection()
        self.load_lang_selection()

    def on_set_lang(self, combo, lang_id):
        if lang_id > 1:
            return

        lng = combo.get_active_text()
        old_lng = self.app.user_data['language'][lang_id]
        if lng != old_lng:
            self.app.user_data['language'][lang_id] = lng
            self.app.gui.vocab_view.refresh_list()
            self.app.save_user()
            self.app.gui.lock_task()

    def load_lang_selection(self):
        self.task_drop.set_active(list(Language.MAP.keys()).index(self.app.get_task_lang()))
        self.ans_drop.set_active(list(Language.MAP.keys()).index(self.app.get_ans_lang()))

    def update_grammar_selection(self, button, name):
        self.app.user_data['grammar'][name] = button.get_active()
        self.app.save_user()

    def load_grammar_selection(self):
        for btn in self.check_list:
            if self.app.user_data['grammar'].get(btn) is not None:
                self.check_list[btn].set_active(self.app.user_data['grammar'][btn])
