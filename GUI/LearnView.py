import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk
from Learn import Task


# map display name to GF type
CAT_MAP = {'Nouns': 'Base_N', 'Verbs': 'Base_V', 'Adjectives': 'Base_A',
           'Adverbs': 'Predicate_Adv', 'Present': 'Pres_S', 'Past': 'Past_S', 'Negative Present': 'PresNeg_S',
           'Negative Past': 'PastNeg_S', 'Future': 'Fut_S', 'Negative Future': 'FutNeg_S',
           'Imperative': 'Imperative_Imp', 'Question': 'Question_QS'}


# noinspection PyCallByClass,PyUnusedLocal
class Box(Gtk.Box):
    # display a text as task for user
    task_label = Gtk.Label()
    # display feedback for user's entry
    entry_label = Gtk.Label()
    # entry field for users answer
    task_entry = Gtk.Entry()
    task_button = Gtk.Button(label='>')

    # needs GF to pass input, vocab_view to get checkboxes
    # noinspection PyCallByClass
    def __init__(self, app):
        self.tm = app.task_manager
        self.grammar_selection = app.user_data.get('grammar')
        self.app = app

        # noinspection PyCallByClass
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL, spacing=6)

        center_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.set_center_widget(center_box)

        self.task_label.set_markup('<big>click ></big>')
        center_box.pack_start(self.task_label, False, False, 0)

        text_button_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        text_button_box.set_name('TextButtonBox')
        Gtk.StyleContext.add_class(text_button_box.get_style_context(), "linked")

        self.task_entry.set_width_chars(50)
        self.task_entry.set_alignment(0.53)
        self.task_entry.connect('key-release-event', self.on_key_released)
        self.task_button.connect('clicked', self.on_submit_clicked)
        self.task_button.set_tooltip_text('submit answer, or leave empty to skip')

        text_button_box.pack_start(self.task_entry, True, True, 0)
        text_button_box.pack_start(self.task_button, False, True, 0)
        center_box.pack_start(text_button_box, False, False, 0)
        center_box.pack_start(self.entry_label, False, False, 0)

    def on_key_released(self, widget, ev, data=None):
        if ev.keyval == Gdk.KEY_Return and self.app.get_ans_lang() != 'Jpn':
            self.on_submit_clicked(None)

    def on_submit_clicked(self, button):
        self.app.gui.vocab_view.refresh_list()
        if not self.app.vocab.check_complete((self.app.get_task_lang(), self.app.get_ans_lang())):
            self.task_label.set_markup('<big>incomplete VOCAB</big>')
            return

        if self.app.gf.is_compiling():
            self.task_label.set_markup('<big>compiling ...</big>')
            return

        # remove special characters.
        entry_input = self.task_entry.get_text().replace('?', '').replace('.', '').replace('　', ' ')

        cats = []
        if self.grammar_selection:
            for btn in self.grammar_selection:
                if self.grammar_selection[btn]:
                    cats.append(CAT_MAP[btn])

        # enable skip
        if entry_input == '':
            self.task_label.set_markup('<big>generating ...</big>')
            new_task_txt = self.tm.get_new_task(cats)
            if self.tm.current_task['tree'].split(' ')[0] in ('question', 'say_QS'):
                new_task_txt += '?'
            self.task_label.set_markup('<big>' + new_task_txt + '</big>')
            self.entry_label.set_text('')
            return

        feedback = self.tm.give_answer(entry_input)
        if not feedback['correct']:
            # create pretty feedback markup
            marked_entry = ''
            for i, fb in enumerate(feedback['words']):
                if not fb:
                    marked_entry += '_ '
                    continue

                if not fb[Task.DIST] == 0:
                    marked_entry += '<b>'

                for j, d in enumerate(Task.mk_answer_dif(fb[Task.WORD], fb[Task.FIT])):
                    if j > len(fb[Task.WORD]) - 1:
                        c = ' '
                    else:
                        c = fb[Task.WORD][j]
                    if not d:
                        marked_entry += '<span background="red">' + c + '</span>'
                    else:
                        marked_entry += c

                if not fb[Task.DIST] == 0:
                    marked_entry += '</b>'
                marked_entry += ' '

            if feedback['sentence'] and feedback['sentence'][0] != feedback['sentence'][1]:
                marked_entry = '<u>' + marked_entry + '</u>'

            self.entry_label.set_markup('<big>' + marked_entry + '</big>')
            return

        # generate new task if correct answer
        else:
            self.task_label.set_markup('<big>generating ...</big>')
            new_task_txt = self.tm.get_new_task(cats)
            if self.tm.current_task['tree'].split(' ')[0] in ('question', 'say_QS'):
                new_task_txt += '?'
            self.task_label.set_markup('<big>' + new_task_txt + '</big>')
            self.task_entry.set_text('')
            self.entry_label.set_text('')
