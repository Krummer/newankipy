import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from Learn import Vocab
from GF import Language


class Dialog(Gtk.Dialog):
    def __init__(self, app, word=None):
        Gtk.Dialog.__init__(self, "New Word", app.gui, 0)
        self.app = app
        self.set_modal(True)
        self.add_button('confirm', 27)
        self.set_resizable(False)

        hb = Gtk.HeaderBar()
        hb.set_show_close_button(True)
        self.set_titlebar(hb)

        views = Gtk.Stack()

        type_label = Gtk.Label('Type')
        type_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        type_box.pack_start(type_label, True, True, 0)
        self.type_drop = Gtk.ComboBoxText()
        type_box.pack_end(self.type_drop, True, True, 0)
        self.type_drop.connect("changed", self.on_type)

        for item in Language.MAP[self.app.get_ans_lang()]:
            self.type_drop.append_text(item)

        # make dialog page for known and learning lang
        self.lang_dialogs = {}
        for lang in (self.app.get_ans_lang(), self.app.get_task_lang()):
            self.lang_dialogs[lang] = {}
            # view is the container for the language specific page
            self.lang_dialogs[lang]['view'] = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=1)
            views.add_titled(self.lang_dialogs[lang]['view'], lang, lang)

            # arguments listbox
            self.lang_dialogs[lang]['listbox'] = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=1)
            self.lang_dialogs[lang]['view'].add(self.lang_dialogs[lang]['listbox'])

            # prepare subtype drop down box
            subtype_label = Gtk.Label('Case')
            subtype_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
            self.lang_dialogs[lang]['subtype_drop'] = Gtk.ComboBoxText()
            self.lang_dialogs[lang]['subtype_drop'].connect("changed", self.on_sub_type, lang)
            subtype_box.pack_end(self.lang_dialogs[lang]['subtype_drop'], True, True, 0)
            subtype_box.pack_start(subtype_label, False, False, 0)
            self.lang_dialogs[lang]['listbox'].add(subtype_box)

            fields_label = Gtk.Label()
            fields_label.set_markup('<b>Required Fields</b>')
            self.lang_dialogs[lang]['view'].add(fields_label)

            # create input field list box
            self.lang_dialogs[lang]['sub_listbox'] = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=1)
            self.lang_dialogs[lang]['view'].add(self.lang_dialogs[lang]['sub_listbox'])


            for base_type in Language.MAP[lang]:
                for sub_type in Language.MAP[lang][base_type]:
                    for arg in Language.MAP[lang][base_type][sub_type]:
                        # handle drop downs
                        if isinstance(arg, tuple) and not self.lang_dialogs[lang].get(arg[0] + '_entry'):
                            entry_key = arg[0] + '_entry'
                            box_key = arg[0] + '_box'
                            self.lang_dialogs[lang][entry_key] = Gtk.ComboBoxText()

                            for option in arg[1:]:
                                self.lang_dialogs[lang][entry_key].append_text(option)

                            self.lang_dialogs[lang][box_key] = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL,
                                                                       spacing=10)
                            entry_label = Gtk.Label(arg[0])
                            self.lang_dialogs[lang][box_key].pack_start(entry_label, True, False, 10)
                            self.lang_dialogs[lang][box_key].pack_end(self.lang_dialogs[lang][entry_key],
                                                                      True, True, 0)
                            self.lang_dialogs[lang]['sub_listbox'].pack_end(self.lang_dialogs[lang][box_key], True, True, 0)
                        # add each simple input field
                        elif isinstance(arg, str) and not self.lang_dialogs[lang].get(arg + '_entry'):
                            arg_label = Gtk.Label(arg)
                            self.lang_dialogs[lang][arg + '_box'] = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL,
                                                                            spacing=10)
                            self.lang_dialogs[lang][arg + '_entry'] = Gtk.Entry()
                            self.lang_dialogs[lang][arg + '_entry'].set_width_chars(20)
                            self.lang_dialogs[lang][arg + '_box'].pack_start(arg_label, True, False, 0)
                            self.lang_dialogs[lang][arg + '_box'].pack_end(self.lang_dialogs[lang][arg + '_entry'],
                                                                           False, False, 0)
                            self.lang_dialogs[lang]['sub_listbox'].add(self.lang_dialogs[lang][arg + '_box'])

        view_switcher = Gtk.StackSwitcher()
        view_switcher.set_stack(views)
        hb.pack_start(type_box)
        hb.pack_start(view_switcher)
        self.get_content_area().pack_start(views, True, True, 0)

        self.show_all()

        # add field for notes
        self.notes_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=1)
        notes_label = Gtk.Label()
        notes_label.set_markup('<b>Notes</b>')
        self.notes_entry = Gtk.Entry()
        self.notes_box.add(notes_label)
        self.notes_box.add(self.notes_entry)
        self.get_content_area().pack_end(self.notes_box, True, True, 5)

        # add fields for semantics
        self.sem_listbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=1)
        self.get_content_area().pack_end(self.sem_listbox, True, True, 5)
        sem_header = Gtk.Label()
        sem_header.set_markup('<b>Semantics</b>')
        self.sem_listbox.add(sem_header)

        self.sem_label = Gtk.Label('Subject')
        self.sem_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.sem_entry = Gtk.Entry()
        self.sem_box.pack_start(self.sem_label, False, False, 0)
        self.sem_box.pack_end(self.sem_entry, True, True, 0)
        self.sem_listbox.add(self.sem_box)

        obj_label = Gtk.Label('Object')
        self.obj_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.obj_entry = Gtk.Entry()
        self.obj_box.pack_start(obj_label, False, False, 0)
        self.obj_box.pack_end(self.obj_entry, True, True, 0)
        self.sem_listbox.add(self.obj_box)

        for lang in (self.app.get_ans_lang(), self.app.get_task_lang()):
            self.lang_dialogs[lang]['view'].hide()

        if word or word == 0:
            # fill the fields with the available information
            word_args = self.app.vocab.lexicon[word][Vocab.ARGS]
            for lang in word_args:
                if lang not in (self.app.get_ans_lang(), self.app.get_task_lang()):
                    continue

                self.type_drop.set_active(word_args[lang][0])
                self.lang_dialogs[lang]['subtype_drop'].set_active(word_args[lang][1])

                active_type = self.type_drop.get_active_text()
                active_subtype = self.lang_dialogs[lang]['subtype_drop'].get_active_text()
                for arg in Language.MAP[lang][active_type][active_subtype]:
                    # offset by 2, because idx 0 and 1 are for types
                    arg_idx = Language.MAP[lang][active_type][active_subtype].index(arg) + 2

                    # handle dropdowns
                    if not isinstance(arg, str):
                        option_list = arg[1:]
                        self.lang_dialogs[lang][arg[0] + '_entry'].set_active(option_list.index(word_args[lang][arg_idx]))
                        continue
                    self.lang_dialogs[lang][arg + '_entry'].set_text(word_args[lang][arg_idx])

            word_sem = self.app.vocab.lexicon[word][Vocab.SEM]
            if word_sem and word_sem[0]:
                self.sem_entry.set_text(' '.join(word_sem[0]))
            if len(word_sem) == 2 and word_sem[1]:
                self.obj_entry.set_text(' '.join(word_sem[1]))

            if len(self.app.vocab.lexicon[word]) > Vocab.NOTES:
                self.notes_entry.set_text(self.app.vocab.lexicon[word][Vocab.NOTES])

    def on_type(self, combo):
        # hide sem for now and reset text
        self.sem_listbox.hide()
        self.sem_entry.set_text('')
        self.obj_entry.set_text('')

        for lang in self.lang_dialogs:
            # show lang views
            self.lang_dialogs[lang]['view'].show()
            self.lang_dialogs[lang]['listbox'].show()
            self.lang_dialogs[lang]['sub_listbox'].hide()
            # if there are subtypes, provide drop down
            word_type = self.type_drop.get_active_text()
            if any(Language.MAP[lang][word_type]):
                self.lang_dialogs[lang]['subtype_drop'].remove_all()
                for item in Language.MAP[lang][word_type]:
                    self.lang_dialogs[lang]['subtype_drop'].append_text(item)

    # construct input fields for specific types (regular, irregular ...)
    def on_sub_type(self, combo, lang):
        subtype = combo.get_active_text()
        if not subtype:
            return

        word_type = self.type_drop.get_active_text()
        for item in self.lang_dialogs[lang]['sub_listbox'].get_children():
            item.hide()

        for arg in Language.MAP[lang][word_type][subtype]:
            if isinstance(arg, tuple):
                self.lang_dialogs[lang][arg[0] + '_box'].show()
            else:
                self.lang_dialogs[lang][arg + '_box'].show()
        self.lang_dialogs[lang]['sub_listbox'].show()

        self.notes_box.show_all()

        # show hide sem entries
        if word_type in 'VN':
            self.sem_listbox.show_all()
            self.obj_box.hide()
            if word_type == 'N':
                self.sem_label.hide()
        elif word_type == 'V2':
            self.sem_listbox.show_all()
        else:
            self.sem_listbox.hide()
