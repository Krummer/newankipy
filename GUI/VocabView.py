from datetime import datetime
from threading import Thread, Timer
from Core.Helper import str_to_time
from Learn import Vocab
from GF import Language
from . import Word

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GLib


# noinspection PyCallByClass
class Box(Gtk.Box):
    # needs app for saving grammar selection
    def __init__(self, app):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self.app = app

        # the box with the words
        self.word_store = Gtk.ListStore(int, str, str, str, str, str)
        header_tags = ('#', 'type', 'task', 'answer', 'notes','due in')
        self.word_box = Gtk.TreeView(self.word_store)
        renderer = Gtk.CellRendererText()
        for i, tag in enumerate(header_tags):
            column = Gtk.TreeViewColumn(tag, renderer, text=i)
            self.word_box.append_column(column)

        self.fill_vocab_grid()
        scrolled = Gtk.ScrolledWindow()
        scrolled.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        scrolled.add(self.word_box)
        self.pack_end(scrolled, True, True, 0)

        self.word_box.connect('key-release-event', self.on_key_released)

        # settings box
        btn_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        new_word_btn = Gtk.Button(None, image=Gtk.Image(stock=Gtk.STOCK_ADD))
        new_word_btn.connect('clicked', self.on_new_word)
        new_word_btn.set_tooltip_text('add a new word')
        self.export_btn = Gtk.Button(None, image=Gtk.Image(stock=Gtk.STOCK_REFRESH))
        self.export_btn.connect('clicked', self.on_export)
        self.export_btn.set_tooltip_text('compile')
        edit_btn = Gtk.Button(None, image=Gtk.Image(stock=Gtk.STOCK_EDIT))
        edit_btn.connect('clicked', self.on_word_edit)
        edit_btn.set_tooltip_text('edit the selected word')
        self.del_btn = Gtk.Button(None, image=Gtk.Image(stock=Gtk.STOCK_REMOVE))
        self.del_btn.connect('clicked', self.on_word_del)
        self.del_btn.set_tooltip_text('delete selected word (needs recompile)')

        btn_box.pack_start(new_word_btn, False, True, 0)
        btn_box.pack_start(edit_btn, False, True, 0)
        btn_box.pack_end(self.export_btn, False, True, 0)
        btn_box.pack_start(self.del_btn, False, True, 0)

        self.pack_start(btn_box, False, True, 0)

        # run due time update thread
        list_refresh = RefreshThread(self.update_callback)
        list_refresh.start()

    def update_callback(self):
        GLib.idle_add(self.update_list_time)

    def update_list_time(self):
        for row in self.word_store:
            row[5] = self.mk_due_str(row[0])

    # refresh due time in list
    def refresh_list(self):
        self.word_store.clear()
        self.fill_vocab_grid()

    # make str for due time
    def mk_due_str(self, word_idx):
        word = self.app.vocab.lexicon[word_idx]
        dated = str_to_time(word[Vocab.SRS][0]) - datetime.now()
        if dated.days < 0:
            due = 'now'
        elif dated.days == 0:
            sec = dated.seconds
            mins = int(sec / 60)
            hours = int(mins / 60)
            if hours > 0:
                mins -= 60 * hours
                if mins > 0:
                    due = str(hours) + ' h ' + str(mins) + ' min'
                else:
                    due = str(hours) + ' h '
            elif mins > 0:
                due = str(mins) + ' min'
            else:
                due = '< 1 min'
        else:
            due = str(dated.days) + ' day(s)'
        return due

    # inserts all vocab into the word list.
    def fill_vocab_grid(self):
        if not self.app.vocab.lexicon:
            return
        for idx, word in enumerate(self.app.vocab.lexicon):
            self.add_word_to_tree(idx, word)

    def add_word_to_tree(self, word_idx, word=None):
        if not word:
            word = self.app.vocab.lexicon[word_idx]

        # get lins for selected langs.
        task_info = word[Vocab.ARGS].get(self.app.get_task_lang())
        if not task_info:
            task_info = ' '
        else:
            task_info = task_info[2]

        ans_info = word[Vocab.ARGS].get(self.app.get_ans_lang())
        if not ans_info:
            ans_info = ' '
        else:
            ans_info = ans_info[2]

        due = self.mk_due_str(word_idx)
        if len(word) > Vocab.NOTES:
            notes = word[Vocab.NOTES]
        else:
            notes = ''
        self.word_store.append([word_idx, word[Vocab.CAT], task_info, ans_info, notes, due])

    def on_export(self, button):
        if self.app.is_ready():
            button.set_sensitive(False)
            self.app.gui.learn_view.task_button.set_sensitive(False)
            self.app.vocab.make_grammar_files()
            self.app.gf.make_grammar()
            self.app.gui.set_status_text('compiling ...')
        else:
            self.app.gui.set_status_text('missing vocab information')

    def on_new_word(self, button):
        self.button_event()

    def on_word_edit(self, button=None):
        selection = self.word_box.get_selection()
        model, treeiter = selection.get_selected()
        if treeiter:
            self.button_event(model[treeiter][0])

    def on_word_del(self, button=None):
        selection = self.word_box.get_selection()
        model, treeiter = selection.get_selected()
        if treeiter:
            self.app.vocab.remove_vocab(model[treeiter][0])
            self.word_store.remove(treeiter)
        self.app.save_user()
        self.app.gui.lock_task()

    def on_key_released(self, widget, ev, data=None):
        if ev.keyval == Gdk.KEY_Return:
            self.on_word_edit()
        elif ev.keyval == Gdk.KEY_Delete:
            self.on_word_del()

    def button_event(self, word_idx=None):
        # create a dialog
        if word_idx or word_idx == 0:
            word_dialog = Word.Dialog(self.app, word_idx)
        else:
            word_dialog = Word.Dialog(self.app)
        r = word_dialog.run()

        if r == 27:
            # pull data from dialog and put into vocab
            active_type = word_dialog.type_drop.get_active_text()
            if active_type is None:
                print('incomplete input.')
                word_dialog.destroy()
                return

            # word_id = ''
            word_args = {}
            for lang in (self.app.get_ans_lang(), self.app.get_task_lang()):
                active_subtype = word_dialog.lang_dialogs[lang]['subtype_drop'].get_active_text()
                if active_subtype is None:
                    print('incomplete input.')
                    word_dialog.destroy()
                    return
                word_args[lang] = []
                # first 2 arg in list are type
                word_args[lang].append(word_dialog.type_drop.get_active())
                word_args[lang].append(word_dialog.lang_dialogs[lang]['subtype_drop'].get_active())
                for arg in Language.MAP[lang][active_type][active_subtype]:
                    # handle dropdowns
                    if isinstance(arg, tuple):
                        entry = word_dialog.lang_dialogs[lang][arg[0] + '_entry'].get_active_text()
                        if entry is None or entry == '':
                            print('incomplete input.')
                            word_dialog.destroy()
                            return
                        word_args[lang].append(entry)

                        # special fix for JPN counter
                        if lang == 'Jpn' and arg == Language.JPN_COUNTER:
                            word_args['Jpn'].append(str(entry != 'つ'))
                        continue

                    entry = word_dialog.lang_dialogs[lang][arg + '_entry'].get_text()
                    if entry is None or entry == '':
                        print('incomplete input')
                        word_dialog.destroy()
                        return

                    # add all arguments
                    word_args[lang].append(entry)

            word_sem = [[], []]
            if word_dialog.sem_entry.get_text() not in ('', None):
                word_sem[0] = word_dialog.sem_entry.get_text().replace(',', ' ').replace('  ', ' ').strip().split(' ')
                if active_type == 'V2' and word_dialog.obj_entry.get_text() not in ('', None):
                    word_sem[1] = word_dialog.obj_entry.get_text().split(' ')

            notes = word_dialog.notes_entry.get_text()

            if word_idx or word_idx == 0:
                # modify word
                self.app.vocab.add_vocab(active_type, word_args, word_sem, word_idx, notes)
                self.refresh_list()
            else:
                # create new word
                word_idx = self.app.vocab.add_vocab(active_type, word_args, word_sem, notes=notes)
                self.add_word_to_tree(word_idx)

            # add to tree view
            self.app.save_user()
            self.app.gui.lock_task()

        word_dialog.destroy()


class RefreshThread(Thread):
    def __init__(self, refresh_function):
        Thread.__init__(self)
        self.setDaemon(True)
        self.refresh = refresh_function

    def run(self):
        # refresh list every minute to show correct due time
        self.refresh()

        timer = Timer(10.0, self.run)
        timer.setDaemon(True)
        timer.start()