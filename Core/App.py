import json
import os
from GF.Server import Interface
from GUI.MainWindow import Window
from Learn.Task import TaskManager
from Learn.Vocab import VocabManager


# main class
class Application:

    def __init__(self):
        self.config_data = self.load_config()
        self.user_data = self.load_user()

        # modules
        self.gf = Interface(self)
        self.vocab = VocabManager(self)
        self.task_manager = TaskManager(self)
        self.gui = Window(self)

    def get_task_lang(self):
        return self.user_data['language'][1]

    def get_ans_lang(self):
        return self.user_data['language'][0]

    def is_ready(self):
        vocab_rdy = self.vocab.check_complete((self.get_task_lang(), self.get_ans_lang()))
        server_rdy = not self.gf.is_compiling()

        return vocab_rdy and server_rdy

    def load_config(self):
        if os.path.exists('config/app.json'):
            # load session ID
            with open('config/app.json') as config:
                data = json.load(config)
                config.close()
                self.config_data = data
                return data

    def save_config(self):
        if any(self.config_data):
            with open('config/app.json', 'w') as outfile:
                json.dump(self.config_data, outfile)
                outfile.close()

    def load_user(self):
        if self.config_data.get('current_user'):
            if not os.path.exists('config/users/'):
                os.makedirs('config/users/')
            user_file_path = 'config/users/' + self.config_data['current_user'] + '.json'

            if not os.path.exists(user_file_path):
                return {'language': ['Eng', 'Jpn']}
            else:
                with open(user_file_path) as config:
                    data = json.load(config)
                    self.user_data = data
                    return data
        else:
            # set defaults
            return {'language': ['Eng', 'Jpn']}

    def save_user(self):
        if not os.path.exists('config/users/'):
            os.makedirs('config/users/')

        if self.config_data.get('current_user'):
            user_file_path = 'config/users/' + self.config_data['current_user'] + '.json'
            if any(self.user_data):
                with open(user_file_path, 'w') as outfile:
                    json.dump(self.user_data, outfile)
