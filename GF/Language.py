from collections import OrderedDict

# GER
GER_GENDER = ('gender', 'masculine', 'feminine', 'neuter')

# JPN
JPN_GROUP = ('group', 'Gr1', 'Gr2', 'Suru', 'Kuru')
JPN_PARTICLE = ('object particle', 'を', 'に', 'と')
JPN_ANIMATE = ('animateness', 'Anim', 'Inanim')
JPN_COUNTER = ('counter', 'つ', '本', '匹', '枚', '部', '台', '杯', '回', '人', '名', '冊', '話', '面', '個')

# types and needed arguments defined by GF resource grammar
# if a lang dict is empty, only provide a single input field in UI
# gender will be made a drop down
# OrderedDicts are used to make some GUI work easier.
MAP = OrderedDict([
    ('Eng', OrderedDict([
        ('A', OrderedDict([
            ('regular', ('basic form',)),
            ('irregular comparative', ('basic form', 'comparative')),
            ('completely irregular', ('basic form', 'comparative', 'superlative', '(alt.) basic form'))
        ])),
        ('V', OrderedDict([
            ('regular', ('basic form',)),
            ('irregular', ('basic form', 'past', 'perfect')),
            ('conso. duplication', ('basic form', 'sg past')),
            ('completely irregular', ('basic form', '2. Sg', '2. Sg Past', 'perfect', 'progressive'))
        ])),
        ('N', OrderedDict([
            ('plural s', ('nom',)),
            ('irregular plural', ('sg', 'pl')),
            ('irregular genitives', ('sg', 'pl', 'gen sg', 'gen pl'))
        ]))
    ])),

    ('Ger', OrderedDict([
        ('A', OrderedDict([
            ('regular', ('basic form',)),
            ('irregular comparative', ('positive', 'comparative', 'superlative'))
        ])),
        ('V', OrderedDict([
            ('regular', ('basic form',)),
            ('irregular', ('basic form', '2. Sg', '2. Sg Past', '1. Conj Past', 'Past participle: ge-')),
            ('completely irregular', ('basic form', '2. Sg', 'Imperative',
                                      '2. Sg Past', '1. Conj', 'Past participle'))
        ])),
        ('N', OrderedDict([
            ('plural ending: -e or -n', ('nom',)),
            ('completely irregular', ('nom', 'acc', 'dat', 'gen', 'nom pl', 'dat pl', GER_GENDER)),
            ('masc: -e, neutr: -er, fem: -en', ('nom', GER_GENDER)),
            ('plural ending: -er', ('nom', 'nom pl', GER_GENDER))
        ]))
    ])),

    ('Jpn', OrderedDict([
        ('A', OrderedDict([
            ('regular', ('stem + いorな',))
        ])),
        ('V', OrderedDict([
            ('all', ('plain form', JPN_GROUP))
        ])),
        ('V2', OrderedDict([
            ('all', ('plain form', JPN_PARTICLE, JPN_GROUP))
        ])),
        ('N', OrderedDict([
            ('regular', ('plain form', JPN_ANIMATE)),
            ('formal prefix', ('plain form', 'formal form', JPN_ANIMATE)),
            ('regular+counter', ('plain form', JPN_ANIMATE, JPN_COUNTER)),
            ('formal+counter', ('plain form', 'formal form', JPN_ANIMATE, JPN_COUNTER)),
            ('foreign plural', ('singular', JPN_ANIMATE, JPN_COUNTER, 'plural'))

        ]))
    ]))
])

# determiner for NP generation
DET = ['The', 'Some', 'All', 'No', 'Generic', 'This', 'Many', 'Few']

# placeholder NPs
NP_ANIM = ['NP_Nobody', 'NP_Somebody', '(anaphor I)', '(anaphor He)' , '(anaphor She)', '(anaphor YouSg)', '(anaphor YouPl)']
NP_INANIM = ['NP_Nothing', 'NP_Something' , 'NP_That', 'NP_These', 'NP_This', 'NP_Those', '(anaphor It)']

# some language's rules for v2 are the same ...
for lang in ('Ger', 'Eng'):
    MAP[lang]['V2'] = MAP[lang]['V']

# probably have to add the keywords to grammar by hand as baseline to avoid some clashing.
KEYWORDS = ('feminine', 'masculine', 'neuter', 'True', 'False', 'Gr1', 'Gr2', 'Suru', 'Kuru', 'Anim', 'Inanim')

# helper to convert type strings
TYPE_NAMEtoGF = {'N': 'Base_N', 'A': 'Base_A', 'V': 'Base_V', 'V2': 'Relation_V2', 'Adv': 'Predicate_Adv'}
TYPE_GFtoNAME = {'Base_N': 'N', 'Base_A': 'A', 'Base_V': 'V', 'Relation_V2': 'V2', 'Predicate_Adv': 'Adv'}