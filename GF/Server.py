from random import shuffle
from threading import Thread
from . import Language
import requests


class Interface:

    core_path = './GF/grammar/'

    def __init__(self, app):
        self.app = app
        self.server_address = app.config_data['server_address']
        self.server_port = app.config_data['server_port']

        if not app.config_data.get('current_user'):
            self.mk_gf_usr()
            self.app.config_data['current_user'] = self.user_session.replace('/tmp/gfse.', '')
            self.app.save_config()
            self.app.save_user()

        else:
            self.user_session = '/tmp/gfse.' + self.app.config_data['current_user']

        self.pgf_path = self.get_server_url() + self.user_session + '/Words.pgf'
        print('set server path to: ' + self.pgf_path)
        self.upload_thread = None

    # ------------------------ COMMON INTERACTION -------------------------------
    # returns a random tree (as string)
    def get_random_tree(self, category):
        # shuffle categories for random results
        shuffle(category)

        # request more trees to reduce number of server calls
        print('requesting random tree ...')
        trees = requests.get(self.pgf_path, params={'command': 'random', 'cat': category[0]}).json()

        # in case there was nothing.
        if len(trees) == 0:
            return self.get_random_tree(category)

        for tree in trees:
            # check tree for issues
            next_tree = False
            final_texts = {}
            for lin in tree['linearizations']:
                if len(lin['texts']) == 0:
                    next_tree = True
                else:
                    # clean texts
                    lang_key = lin['to'].replace('Words', '')
                    final_texts[lang_key] = []
                    for item in lin['texts']:
                        if item != '' and '[' not in item:
                            final_texts[lang_key].append(item)
                    if len(final_texts[lang_key]) == 0:
                        next_tree = True
            if next_tree:
                continue
            elif any(final_texts):
                return {'tree': clean_tree(tree['tree']), 'lins': final_texts}

        # last resort, generate a new tree
        return self.get_random_tree(category)

    # request json tree and make it a dictionary
    def get_dict_tree(self, string_tree):
        r = requests.get(self.pgf_path, params={'command': 'abstrjson', 'tree': string_tree})
        return r.json()

    # returns lins for all available languages
    def get_lin(self, string_tree):
        r = requests.get(self.pgf_path, params={'command': 'linearize', 'tree': string_tree})

        # cleanup result
        result = {}
        for lang in r.json():
            lang_key = lang['to'].replace('Words', '')
            result[lang_key] = []
            result[lang_key].append(lang['text'])

        return result

    # returns tree(s) fitting the input text.
    def get_tree_parse(self, text_input):
        r = requests.get(self.pgf_path, params={'command': 'parse', 'input': text_input, 'cat': 'Utterance_Utt'})

        # cleanup result
        result = {}
        for lang in r.json():
            lang_key = lang['from'].replace('Words', '')
            result[lang_key] = []
            if lang.get('trees'):
                result[lang_key] = [clean_tree(x) for x in lang['trees']]
        return result

    # ------------------------ SERVER MAGIC -------------------------------------
    # get full http url
    def get_server_url(self):
        return 'http://' + self.server_address + ':' + self.server_port

    # upload grammar and compile
    def make_grammar(self):
        if self.upload_thread and self.upload_thread.is_alive():
            return

        # upload core again
        self.upload_core()

        files = {}
        src_files = ['Words']
        for lng in (self.app.get_task_lang(), self.app.get_ans_lang()):
            src_files.append('Words' + lng)

        for item in src_files:
            filepath = self.core_path + item + '.gf'
            with open(filepath, 'r') as gf_source:
                files[item + '.gf'] = gf_source.read()

        # delete old pgf file to make sure language switching has effect
        self.rm_file('Words.pgf')

        # upload core grammar
        command = {'command': 'make', 'dir': self.user_session}
        args = merge_request_args(base=command, paths=files)

        self.upload_thread = GrammarThread(self.get_server_url() + '/cloud', args, self.app.gui.ready_callback)
        self.upload_thread.start()

    # generate user on server
    def mk_gf_usr(self):
        r = requests.get(self.get_server_url() + '/new')
        self.user_session = r.text
        self.upload_core()

    def upload_core(self):
        files = {}
        # list all core files
        core_files = ['Core', 'CoreI']
        for lng in Language.MAP:
            core_files.append('Core' + lng)

        for item in core_files:
            file_path = self.core_path + item + '.gf'
            gf_source = open(file_path, 'r')
            files[item + '.gf'] = gf_source.read()
            gf_source.close()

        # upload core grammar
        command = {'command': 'upload',
                   'dir': self.user_session}

        args = merge_request_args(base=command, paths=files)
        self.upload_thread = GrammarThread(self.get_server_url() + '/cloud', args)
        self.upload_thread.start()

    # if the need arises to delete something on the server.
    def rm_file(self, file):
        rm_command = '&command=rm&file=' + file
        requests.get(self.get_server_url() + '/cloud?dir=' + self.user_session + rm_command)

    def is_compiling(self):
        return bool(self.upload_thread and self.upload_thread.is_alive())


def merge_request_args(base, paths):
    z = paths.copy()
    z.update(base)
    return z


def clean_tree(tree_string):
    if tree_string.startswith('say'):
        # remove say function and last bracket
        tree_string = tree_string[:-1]
        tree_list = tree_string.split('(')
        tree = '('.join(tree_list[1:])
        return tree
    else:
        return tree_string


class GrammarThread(Thread):

    def __init__(self, url, args, callback=None):
        Thread.__init__(self)
        self.command_url = url
        self.args = args
        self.callback = callback

    def run(self):
        # make grammar
        r = requests.get(self.command_url, params=self.args)
        print('Compile Status: ' + str(r.status_code))

        if self.callback:
            self.callback()
