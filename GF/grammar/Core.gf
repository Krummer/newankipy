abstract Core = {
-- complete core grammar for all supported grammatical things.
 cat

    Anaphor_Pron;

    Base_N;
    Base_V;
    Base_A;

    Entity_NP;

    Predicate_CN;
    Predicate_VP;
    Predicate_AP;
    Predicate_Adv;

    Relation_N2;
    Relation_V2;
    Relation_A2;
    Relation_Prep;

    Complement_VV;
    Complement_AdA;
    Complement_Comp;
    Conjunction_Conj;
    Subjunction_Subj;
    Relative_RP;
    Relative_RS;
    Relative_RCl;

    Question_QS;
    Imperative_Imp;
    Statement_Cl;
    Utterance_Utt;
    Pres_S;
    Past_S;
    Fut_S;
    PresNeg_S;
    PastNeg_S;
    FutNeg_S;
    Sentence_S;


 fun


    --------------------
    ---- Operations ----
    --------------------

    ---- Type Conversion
    mk_N_CN  : Base_N -> Predicate_CN;
    mk_V_VP  : Base_V -> Predicate_VP;
    mk_A_AP  : Base_A -> Predicate_AP;
    mk_A_Adv : Base_A -> Predicate_Adv;

    mk_AP_VP    : Predicate_AP    -> Predicate_VP;  --- to be green.
    mk_CN_VP    : Predicate_CN    -> Predicate_VP;
    mk_NP_VP    : Entity_NP       -> Predicate_VP;
    mk_Comp_VP  : Complement_Comp -> Predicate_VP;
    mk_prog     : Predicate_VP    -> Predicate_VP;  --- progressive
    pass_V2     : Relation_V2     -> Predicate_VP;  --- passive
    pass_V2_NP  : Relation_V2     -> Entity_NP    -> Predicate_VP;

    mk_N2_CN    : Relation_N2      -> Entity_NP   -> Predicate_CN;
    mk_A2_AP    : Relation_A2      -> Entity_NP   -> Predicate_AP;
    mk_Prep_Adv : Relation_Prep    -> Entity_NP   -> Predicate_Adv;
    mk_Subj_Adv : Subjunction_Subj -> Sentence_S  -> Predicate_Adv;
    mk_AdA_AP   : Complement_AdA   -> Base_A      -> Predicate_AP;
    mk_V2_VP    : Relation_V2      -> Entity_NP   -> Predicate_VP;

    --- using Base_N to avoid recursion?
    rel_CN      : Predicate_CN   -> Relative_RS   -> Predicate_CN;

    mk_Conj_AP : Conjunction_Conj -> Predicate_AP -> Predicate_AP -> Predicate_AP;
    ---mk_Conj_NP : Conjunction_Conj -> Entity_NP    -> Entity_NP    -> Entity_NP;
    mk_Conj_RS : Conjunction_Conj -> Relative_RS  -> Relative_RS  -> Relative_RS;

    --- Clause Application
    appl_CN : Predicate_CN -> Statement_Cl; -- there is a old thing
    appl_NP : Entity_NP    -> Statement_Cl; -- there are many things
    appl_VP : Predicate_VP -> Statement_Cl; -- it is raining

    appl_NP_NP  : Entity_NP     -> Entity_NP     -> Statement_Cl; --- she is the woman
    appl_NP_CN  : Entity_NP     -> Predicate_CN  -> Statement_Cl; --- she is a thing
    appl_NP_VP  : Entity_NP     -> Predicate_VP  -> Statement_Cl; --- she always sleeps
    appl_NP_AP  : Entity_NP     -> Predicate_AP  -> Statement_Cl; --- she is very old
    appl_NP_Adv : Entity_NP     -> Predicate_Adv -> Statement_Cl; --- she is here

    appl_NP_V2_NP : Entity_NP -> Relation_V2   -> Entity_NP    -> Statement_Cl; --- she loves him
    appl_NP_A_NP  : Entity_NP -> Base_A        -> Entity_NP    -> Statement_Cl; --- she is older than me
    appl_NP_VV_VP : Entity_NP -> Complement_VV -> Predicate_VP -> Statement_Cl; --- she wants to sleep

    --- Relative Application
    rappl_VP    : Relative_RP -> Predicate_VP  -> Relative_RCl;
    rappl_AP    : Relative_RP -> Predicate_AP  -> Relative_RCl;
    rappl_NP    : Relative_RP -> Entity_NP     -> Relative_RCl;
    rappl_CN    : Relative_RP -> Predicate_CN  -> Relative_RCl;
    rappl_Adv   : Relative_RP -> Predicate_Adv -> Relative_RCl;
    rappl_VV_VP : Relative_RP -> Complement_VV -> Predicate_VP -> Relative_RCl;

    ---- Modification (first argument is the modifier, second one the modified)
    modify_AP_CN  : Predicate_AP   -> Predicate_CN -> Predicate_CN;
    modify_Adv_VP : Predicate_Adv  -> Predicate_VP -> Predicate_VP;


    ---------------------
    ---- Expressions ----
    ---------------------

    ---- Determiners
    The, Some, All, No, Generic, This, Many, Few: Predicate_CN -> Entity_NP;

    ---- Anaphors
    I, We, YouSg, YouPl, He, She, It, They : Anaphor_Pron;
    anaphor : Anaphor_Pron -> Entity_NP;
    poss    : Anaphor_Pron -> Predicate_CN -> Entity_NP;

    --- Relative Entities
    NP_Nobody, NP_Nothing, NP_Somebody, NP_Something, NP_That, NP_These, NP_This, NP_Those : Entity_NP;
    Which   : Relative_RP;
    relPron : Relation_Prep -> Entity_NP -> Relative_RP -> Relative_RP;

    --- Relations
    V2_have : Relation_V2;
    With, Possess, In, From, For, To, Without, After, Before, On, Under, Between : Relation_Prep;

    --- junctions
    And, Or : Conjunction_Conj;
    Because, If, That_J, When : Subjunction_Subj;

    --- Adverbs
    Everywhere, HereFrom, HereTo, Here, Somewhere, ThereFrom, ThereTo, There : Predicate_Adv;

    --- Complements
    Can, Must, Want : Complement_VV;
    Almost, Too, Very : Complement_AdA;
    mk_AP_Comp, mk_NP_Comp, mk_Adv_Comp  : Predicate_AP  -> Complement_Comp;
    ---mk_NP_Comp  : Entity_NP     -> Complement_Comp;
    ---mk_Adv_Comp : Predicate_Adv -> Complement_Comp;

    ---- Semantically light expressions
    relative    : Relative_RCl -> Relative_RS;
    imperative  : Predicate_VP -> Imperative_Imp;
    question    : Statement_Cl -> Question_QS;
    sentence    : Statement_Cl -> Sentence_S;                   --- this is not in Utt, because of targeted gen.

    --- special types for targeted generation.
    present_pos : Statement_Cl -> Pres_S;
    present_neg : Statement_Cl -> PresNeg_S;
    past_pos    : Statement_Cl -> Past_S;
    past_neg    : Statement_Cl -> PastNeg_S;
    future_pos  : Statement_Cl -> Fut_S;
    future_neg  : Statement_Cl -> FutNeg_S;

    ---- Utterances ----
    say_PRESNEG : PresNeg_S       -> Utterance_Utt;
    say_PRES    : Pres_S          -> Utterance_Utt;
    say_PASTNEG : PastNeg_S       -> Utterance_Utt;
    say_PAST    : Past_S          -> Utterance_Utt;
    say_FUTNEG  : FutNeg_S        -> Utterance_Utt;
    say_FUT     : Fut_S           -> Utterance_Utt;
    say_VP      : Predicate_VP    -> Utterance_Utt;
    say_CN      : Predicate_CN    -> Utterance_Utt;
    say_AP      : Predicate_AP    -> Utterance_Utt;
    say_Imp     : Imperative_Imp  -> Utterance_Utt;
    say_QS      : Question_QS     -> Utterance_Utt;

}
