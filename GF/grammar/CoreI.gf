incomplete concrete CoreI of Core = open Syntax in {

 lincat

    Anaphor_Pron  = Pron;

    Base_N        = N;
    Base_V        = V;
    Base_A        = A;

    Entity_NP     = NP;
    Predicate_CN  = CN;
    Predicate_VP  = VP;
    Predicate_AP  = AP;
    Predicate_Adv = Adv;

    Relation_N2   = N2;
    Relation_V2   = V2;
    Relation_A2   = A2;
    Relation_Prep = Prep;

    Complement_VV    = VV;
    Complement_AdA   = AdA;
    Complement_Comp  = Comp;
    Conjunction_Conj = Conj;
    Subjunction_Subj = Subj;

    Relative_RP  = RP;
    Relative_RS  = RS;
    Relative_RCl = RCl;

    Question_QS    = QS;
    Imperative_Imp = Imp;
    Statement_Cl   = Cl;
    Utterance_Utt  = Utt;
    Sentence_S     = S;
    Pres_S         = S;
    Past_S         = S;
    PresNeg_S      = S;
    PastNeg_S      = S;
    FutNeg_S       = S;
    Fut_S          = S;

 lin


    --------------------
    ---- Operations ----
    --------------------

    ---- Type Conversion
    mk_N_CN     n = mkCN n;
    mk_V_VP     v = mkVP v;
    mk_A_AP     a = mkAP a;
    mk_A_Adv    a = mkAdv a;

    mk_AP_VP   a = mkVP a;
    mk_CN_VP   n = mkVP n;
    mk_NP_VP   n = mkVP n;
    mk_Comp_VP c = mkVP c;
    mk_prog    v = progressiveVP v;
    pass_V2    v = passiveVP v;
    pass_V2_NP v n = passiveVP v n;

    mk_N2_CN    r e = mkCN  r e;
    mk_A2_AP    r e = mkAP  r e;
    mk_Prep_Adv r e = mkAdv r e;
    mk_Subj_Adv j s = mkAdv j s;
    mk_AdA_AP   m a = mkAP  m a;
    mk_V2_VP    v n = mkVP  v n;

    rel_CN n r = mkCN n r;

    mk_Conj_AP  c a p = mkAP c a p;
    ---mk_Conj_NP  c n p = mkNP c n p;
    mk_Conj_RS  c r s = mkRS c r s;

    ---- Determiners
    The     p = variants { mkNP the_Det p;
                           mkNP only_Predet (mkNP the_Det p)};
    Some    p = mkNP aSg_Det p;
    All     p = variants { mkNP all_Predet (mkNP aPl_Det p);
                           mkNP every_Det p };
    No      p = mkNP (mkDet no_Quant) p;
    Generic p = mkNP aPl_Det p;
    This    p = variants { mkNP this_Det p;
                           mkNP these_Det p;
                           mkNP those_Det p;
                           mkNP only_Predet (mkNP this_Det p);
                           mkNP only_Predet (mkNP these_Det p);
                           mkNP only_Predet (mkNP those_Det p)};
    Many    p = mkNP many_Det p;
    Few     p = mkNP few_Det p;

    --- Clause Application
    appl_CN n = mkCl n;
    appl_NP n = mkCl n;
    appl_VP v = mkCl v;

    appl_NP_NP  n m = mkCl n m;
    appl_NP_CN  e n = mkCl e n;
    appl_NP_VP  e v = mkCl e v;
    appl_NP_AP  e a = mkCl e a;
    appl_NP_Adv e a = mkCl e a;

    appl_NP_V2_NP n v p   = mkCl n v p;
    appl_NP_A_NP  e1 a e2 = mkCl e1 a e2;
    appl_NP_VV_VP n c v   = mkCl n c v;

    --- Relative Application
    rappl_VP    r v   = mkRCl r v;
    rappl_AP    r a   = mkRCl r a;
    rappl_NP    r n   = mkRCl r n;
    rappl_CN    r n   = mkRCl r n;
    rappl_Adv   r a   = mkRCl r a;
    rappl_VV_VP r c v = mkRCl r c v;

    ---- Modification (first argument is the modifier, second one the modified)
    modify_AP_CN  mod p = mkCN mod p;
    modify_Adv_VP mod p = mkVP p mod;

    ---------------------
    ---- Expressions ----
    ---------------------


    ---- Anaphors
    I     = i_Pron;
    We    = we_Pron;
    YouSg = youSg_Pron;
    YouPl = youPl_Pron;
    He    = he_Pron;
    She   = she_Pron;
    It    = it_Pron;
    They  = they_Pron;
    anaphor a   = mkNP a;
    poss    a p = mkNP a p;

    --- Relations
    V2_have = have_V2;
    With     = with_Prep;
    Possess  = possess_Prep;
    In       = in_Prep;
    From     = from_Prep;
    For      = for_Prep;
    To       = to_Prep;
    Without  = without_Prep;
    After    = after_Prep;
    Before   = before_Prep;
    On       = on_Prep;
    Under    = under_Prep;
    Between  = between_Prep;

    --- Junctions
    And       = and_Conj;
    Or        = or_Conj;
    Because   = because_Subj;
    If        = if_Subj;
    That_J    = that_Subj;
    When      = when_Subj;

    --- Complements
    Can    = can_VV;
    Must   = must_VV;
    Want   = want_VV;
    Almost = almost_AdA;
    Too    = too_AdA;
    Very   = very_AdA;
    mk_AP_Comp  a = mkComp a;
    mk_NP_Comp  n = mkComp n;
    mk_Adv_Comp a = mkComp a;

    --- Relative Entities
    NP_Nobody    = nobody_NP;
    NP_Nothing   = nothing_NP;
    NP_Somebody  = somebody_NP;
    NP_Something = something_NP;
    NP_That      = that_NP;
    NP_These     = these_NP;
    NP_This      = this_NP;
    NP_Those     = those_NP;

    Which         = which_RP;
    relPron p n r = mkRP p n r;

    --- Adverbs
    Everywhere = everywhere_Adv;
    HereFrom   = here7from_Adv;
    HereTo     = here7to_Adv;
    Here       = here_Adv;
    Somewhere  = somewhere_Adv;
    ThereFrom  = there7from_Adv;
    ThereTo    = there7to_Adv;
    There      = there_Adv;


    ---- Semantically light expressions
    relative cl = variants { mkRS presentTense positivePol cl;
                             mkRS presentTense negativePol cl;
                             mkRS pastTense    positivePol cl;
                             mkRS pastTense    negativePol cl;
                             mkRS futureTense  positivePol cl;
                             mkRS futureTense  negativePol cl};

    sentence cl = variants { mkS presentTense positivePol cl;
                             mkS presentTense negativePol cl;
                             mkS pastTense    positivePol cl;
                             mkS pastTense    negativePol cl;
                             mkS futureTense  positivePol cl;
                             mkS futureTense  negativePol cl};

    imperative  p  = mkImp p;
    question    q  = mkQS q;

    present_pos cl = mkS presentTense positivePol cl;
    present_neg cl = mkS presentTense negativePol cl;
    past_pos    cl = mkS pastTense    positivePol cl;
    past_neg    cl = mkS pastTense    negativePol cl;
    future_pos  cl = mkS futureTense  positivePol cl;
    future_neg  cl = mkS futureTense  negativePol cl;

    ---- Utterances ----
    say_PRESNEG   s = mkUtt s;
    say_PRES      s = mkUtt s;
    say_PAST      s = mkUtt s;
    say_PASTNEG   s = mkUtt s;
    say_FUTNEG    s = mkUtt s;
    say_FUT       s = mkUtt s;
    say_VP        p = mkUtt p;
    say_CN        p = mkUtt p;
    say_AP        p = mkUtt p;
    say_Imp       p = mkUtt p;
    say_QS        q = mkUtt q;
}
