#!/bin/bash

gf_path=$(which gf)
if [ -n "$gf_path" ]
then
  gf --server &
  python main.py
  pkill -15 -f "gf --server"
else
  echo "gf not in PATH"
fi
