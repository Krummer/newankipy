#README #

AnkiPy (working title, since there already another ankipy), is a language learning tool mostly inspired by the flashcard tool [Anki](http://ankisrs.net/).
You put in your current learning progress (mostly through vocabulary) and the program will create sentences for you to translate.
The sentence generation is handled by [grammatical framework](http://www.grammaticalframework.org/).

### How do I get set up? ###

* Install [gf](http://www.grammaticalframework.org/download/index.html)
* Install Python3 (I use python3.5, but any version thats not 2 should work)
* Install Gtk3 and gobject-bindings
* Install 'requests' (via pip)

### Usage Info ###

* Start gf in server-mode before running the program (gf --server)
* run main.py
* leave the input field empty to skip a generated sentence
* at least one word needs to be added before generation works (probably)
* also don't check only one type in the settings, if no words of that type exist.

