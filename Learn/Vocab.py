from datetime import datetime
from GF import Language


# idx of category
CAT = 0
# idx of arguments
ARGS = 1
# idx of semantics
SEM = 2
# idx of SRS values
SRS = 3
# idc for notes
NOTES = 4


# vocab management
class VocabManager:

    def __init__(self, app):
        # reference to app for saving lexicon
        self.app = app

        # indicate changes in grammar
        self.changed = False
        # user data must exist now, due to gfintf creating it.
        if app.user_data is not None and app.user_data.get('lexicon') is not None:
            self.lexicon = app.user_data.get('lexicon')
        else:
            self.lexicon = []

    def check_complete(self, langs):
        if len(self.lexicon) == 0:
            return False

        # break at first missing item.
        for word in self.lexicon:
            for lang in langs:
                if not word[ARGS].get(lang):
                    return False

        return True

    # add vocab to dict: id='pizza_CN', cat='Predicate_CN', lins={'Eng': '', 'Ger': ''}, sem=['Edible', 'Baked']
    # sem for v2 etc may be a dict (to manage subject/object slots)
    def add_vocab(self, cat, args, sem, word_idx=None, notes=None):

        if word_idx or word_idx == 0:
            # don't touch srs data if word already exists.
            self.lexicon[word_idx][CAT] = cat
            self.lexicon[word_idx][ARGS].update(args)
            self.lexicon[word_idx][SEM] = sem
            if notes and notes != '':
                if len(self.lexicon[word_idx]) > NOTES:
                    self.lexicon[word_idx][NOTES] = notes
                else:
                    self.lexicon[word_idx].append(notes)
        else:
            new_word = [cat, args, sem, [str(datetime.now()), 0, 0]]
            if notes and notes != '':
                new_word.append(notes)
            self.lexicon.append(new_word)
            word_idx = len(self.lexicon) - 1

        self.changed = True
        print('added word.')

        # returns idx for adding to lists and such.
        return word_idx

    def remove_vocab(self, word_idx):
        del self.lexicon[word_idx]
        self.changed = True
        print('removed word.')

    def make_grammar_files(self):
        # write to the grammar files needed for GF
        with open('GF/grammar/Words.gf', 'w') as words_gf:
            first = True
            print('abstract Words = Core ** {', file=words_gf)
            print('\tfun', file=words_gf)

            for lang in Language.MAP:
                with open('GF/grammar/Words' + lang + '.gf', 'w') as lang_gf:
                    # write header, MAKE SURE EACH LANG HAS ALL IT NEEDS
                    if lang == 'Jpn':
                        print('concrete Words' + lang + ' of Words = Core' + lang + ' ** open Prelude, Syntax' + lang +
                              ', Paradigms' + lang + ', Res' + lang + ' in {', file=lang_gf)
                    else:
                        print('concrete Words' + lang + ' of Words = Core' + lang + ' ** open Syntax' + lang +
                              ', Paradigms' + lang + ' in {', file=lang_gf)
                    print('\tlin', file=lang_gf)

                    # write word entries
                    for idx, word in enumerate(self.lexicon):

                        # filter out words that are missing data
                        if not word[ARGS].get(lang):
                            continue

                        # id needs to start with letter for GF
                        word_id = word[CAT] + '_' + str(idx)

                        # write abstract line
                        if first:
                            print('\t\t' + word_id + ' : ' + Language.TYPE_NAMEtoGF[word[CAT]] + ';', file=words_gf)
                        # write concrete line
                        final_line = convert_to_mk_string(lang, word, word_id)
                        print('\t\t' + final_line, file=lang_gf)

                    first = False

                    # close files
                    print('}', file=lang_gf)
            print('}', file=words_gf)

        # write user data, to keep stuff in sync
        self.app.user_data['lexicon'] = self.lexicon
        self.app.save_user()
        self.changed = False


def convert_to_mk_string(lang, word, word_id):
    lang_lin = args_to_lin(word[ARGS][lang])

    convert_map = {
        'N': word_id + ' = ' + 'mkN ' + lang_lin + ';',
        'V': word_id + ' = ' + 'mkV ' + lang_lin + ';',
        'A': word_id + ' = ' + 'mkA ' + lang_lin + ';',
        'V2': word_id + ' = ' + 'mkV2 (mkV ' + lang_lin + ');',
        'Adv': word_id + ' = ' + 'mkAdv ' + lang_lin + ';'
    }

    # Jpn is different ...
    if lang == 'Jpn':
        convert_map['V2'] = word_id + ' = ' + 'mkV2 ' + lang_lin + ';'

    return convert_map[word[CAT]]


# convert list of arguments to lin string
def args_to_lin(args):
    result_str = ''
    for arg in args[2:]:
        # skip the first 2 arg, because those are the types
        word = arg
        # add quotes if not keyword
        if word not in Language.KEYWORDS:
            word = '"' + word + '"'
        # add space to everything but the last arg
        if args.index(arg) != len(args) - 1:
            word += ' '
        result_str += word
    return result_str

