from random import shuffle
from datetime import datetime, timedelta
from . import Vocab
from GF import Language
from Core.Helper import str_to_time, leven


# the word stirng
WORD = 0
# word fitting
FIT = 1
# idx distance
DIST = 2

# growth rate of time between reps.
SRS_GROWTH = 2
# shrink rate for when mistakes are made.
SRS_SHRINK = 0.5


# generate feedback and stuff
class TaskManager:
    def __init__(self, app):
        self.app = app
        self.current_task = {}

    # returns the task sentence
    def get_new_task(self, cats):
        if not cats:
            cats = ['Utterance_Utt']

        self.current_task = self.app.gf.get_random_tree(cats)
        # reduce tree to relevant parts and make a list
        tree_list = self.current_task['tree'].replace('(', '').replace(')', '').split(' ')

        # make adjustments for SRS and semantics
        new_tree = self.fix_tree(tree_list)
        if new_tree:
            self.current_task['lins'] = self.app.gf.get_lin(self.current_task['tree'])

        # fix special cases for german ...
        lins_tmp = self.current_task['lins'].get('Ger')
        if lins_tmp:
            if len(lins_tmp) == 6:
                # only lins 0 and 3 are correct
                lins_tmp = [lins_tmp[0], lins_tmp[3]]
            else:
                lins_tmp = [lins_tmp[0]]
                if 'nicht ein' in lins_tmp[0]:
                    lins_tmp.append(lins_tmp[0].replace('nicht ein', 'kein'))

            self.current_task['lins']['Ger'] = lins_tmp

        # fix special cases for english ...
        lins_tmp = self.current_task['lins'].get('Eng')
        if lins_tmp:
            if len(lins_tmp) == 4 and len(lins_tmp[0].split(' ')) == 1:
                lins_tmp = [lins_tmp[0]]
            for option in lins_tmp:
                if "won't" in option:
                    lins_tmp.append(option.replace("won't", "will not"))
                elif "n't" in option:
                    lins_tmp.append(option.replace("n't", ' not'))

            self.current_task['lins']['Eng'] = lins_tmp

        # fix special cases for japanese ...
        lins_tmp = self.current_task['lins'].get('Jpn')
        if lins_tmp:
            if len(lins_tmp) == 2 and len(lins_tmp[0].split(' ')) == 1:
                # to not get the counter word
                lins_tmp = [lins_tmp[0]]
            elif len(lins_tmp) > 4:
                # only the first 4 are usually complete.
                lins_tmp = lins_tmp[:3]

            # remove incomplete sentences
            min_len = len(lins_tmp[0])
            for option in lins_tmp:
                if len(option) < min_len or option[-1] == 'ば':
                    del lins_tmp[lins_tmp.index(option)]

            self.current_task['lins']['Jpn'] = lins_tmp

        shuffle(self.current_task['lins'][self.app.get_task_lang()])

        print(self.current_task)
        return self.current_task['lins'][self.app.get_task_lang()][0]

    def fix_tree(self, tree_list):
        tree_changed = False
        clean_list = []
        for idx, word in enumerate(tree_list):
            word_split = word.split('_')
            if len(word_split) == 2 and represents_int(word_split[1]):
                word_idx = int(word_split[1])
                word_cat = word_split[0]
                if len(self.app.vocab.lexicon) > word_idx:
                    word_data = self.app.vocab.lexicon[word_idx]
                    if word_cat == word_data[Vocab.CAT]:
                        # the word is a custom one if the index exists and the category fits.
                        clean_list.append(word)
            elif len(word_split) == 2 and word_split[0] == 'NP':
                clean_list.append(word)
            elif word == 'anaphor':
                clean_list.append('('+ word + ' ' + tree_list[idx+1] + ')')

        clean_copy = clean_list.copy()
        # make a dict and list of all due words.
        # the list is for a fast check, if word is due at all
        due_words = {}
        for idx, word in enumerate(self.app.vocab.lexicon):
            word_date = str_to_time(word[Vocab.SRS][0])
            is_due = (datetime.now() - word_date).days >= 0
            if is_due:
                if not due_words.get(word[Vocab.CAT]):
                    due_words[word[Vocab.CAT]] = []
                due_words[word[Vocab.CAT]].append(idx)

        # make a patches for the tree
        patch = []
        # semantics patches
        for idx, item in enumerate(clean_list):
            i_split = item.split('_')

            # governs order of patch applications
            # default: subj, verb, obj
            verb = None
            subject = None
            object = None
            is_passive = False
            # check for V2
            if i_split[0] == 'V2':
                is_passive = 'pass_V2' == tree_list[tree_list.index(item) - 1]
                if idx-1 >= 0:
                    subject = clean_list[idx-1]
                    del clean_copy[clean_copy.index(subject)]
                if len(clean_list) > idx+1:
                    object = clean_list[idx+1]
                    del clean_copy[clean_copy.index(object)]
                if due_words.get('V2') and (int(i_split[1]) not in due_words['V2']):
                    shuffle(due_words['V2'])
                    verb = due_words['V2'][0]
                    patch.append((item, 'V2_' + str(verb)))
                else:
                    verb = int(i_split[1])
                del clean_copy[clean_copy.index(item)]

            # check V
            elif i_split[0] == 'V':
                if idx-1 >= 0:
                    subject = clean_list[idx-1]
                    del clean_copy[clean_copy.index(subject)]

                if due_words.get('V') and (int(i_split[1]) not in due_words['V']):
                    shuffle(due_words['V'])
                    verb = due_words['V'][0]
                    patch.append((item, 'V_' + str(verb)))
                else:
                    verb = int(i_split[1])
                del clean_copy[clean_copy.index(item)]

            if is_passive:
                order = ((subject, 1), (object, 0))
            else:
                order = ((subject, 0), (object, 1))
            for ject, s in order:
                # replace the nouns
                if ject and due_words.get('N'):
                    ject_split = ject.split('_')
                    fit_words = []
                    for w in due_words['N']:
                        for sem in self.app.vocab.lexicon[w][Vocab.SEM][0]:
                            if sem in self.app.vocab.lexicon[verb][Vocab.SEM][s]:
                                fit_words.append(w)

                    if len(fit_words) > 0:
                        shuffle(fit_words)
                        shuffle(Language.DET)
                        # we got relative pronoun or so
                        if ject_split[0] == 'NP' or 'anaphor' in ject_split[0]:
                            mk_np = '(' + Language.DET[0] + ' (mk_N_CN ' + 'N_' + str(fit_words[0]) + '))'
                            patch.append((ject, mk_np))
                            del due_words['N'][due_words['N'].index(fit_words[0])]
                        elif int(ject_split[1]) not in fit_words:
                            patch.append((ject, 'N_' + str(fit_words[0])))
                            del due_words['N'][due_words['N'].index(fit_words[0])]

        # replace due A and N
        for idx, item in enumerate(clean_copy):
            i_split = item.split('_')
            if i_split[0] == 'A' and due_words.get('A'):
                if int(i_split[1]) not in due_words['A']:
                    shuffle(due_words['A'])
                    patch.append((item, 'A_' + str(due_words['A'][0])))
                    del due_words['A'][0]
            elif i_split[0] == 'N' and due_words.get('N'):
                if int(i_split[1]) not in due_words['N']:
                    shuffle(due_words['N'])
                    patch.append((item, 'N_' + str(due_words['N'][0])))
                    del due_words['N'][0]
            elif due_words.get('N') and (i_split[0] == 'NP' or 'anaphor' in i_split[0]):
                shuffle(due_words['N'])
                mk_np = '(' + Language.DET[0] + ' (mk_N_CN ' + 'N_' + str(due_words['N'][0]) + '))'
                patch.append((item, mk_np))
                del due_words['N'][0]

        # apply patch
        for rule in patch:
            if rule[0] != rule[1]:
                self.current_task['tree'] = self.current_task['tree'].replace(rule[0], rule[1], 1)
                tree_changed = True

        return tree_changed

    def give_answer(self, string):
        # returns correctness of task and feedback things in a list
        # gui has to decide how to display the feedback
        string = string.replace('?', '').replace('.', '').strip()

        # fast check
        correct = string in self.current_task['lins'][self.app.get_ans_lang()]
        if not correct:
            # slow check, parse and compare trees
            ans_parse = self.app.gf.get_tree_parse(string)[self.app.get_ans_lang()]
            task_parse = self.app.gf.get_tree_parse(self.current_task['lins'][self.app.get_task_lang()])[self.app.get_task_lang()]

            # check if any of the trees match, since there is some heavy ambiguity in jpn
            for ans_tree in ans_parse:
                correct = ans_tree in task_parse
                if correct:
                    # set the used tree, so that the correct words due time gets adjusted.
                    self.current_task['tree'] = ans_tree
                    break

            # still not correct, try to find closest matching possible tree
            if not correct:
                lowest_tree = None
                lowest_lin = None
                lowest_leven = None
                for tree in task_parse:
                    tree_lin = self.app.gf.get_lin(tree)
                    lin_task_ans = tree_lin[self.app.get_ans_lang()]
                    tree_min_leven = leven(string, lin_task_ans[0])
                    tree_min_lin = lin_task_ans[0]
                    if len(lin_task_ans) > 1:
                        for lin in lin_task_ans[1:]:
                            new_leven = leven(string, lin)
                            if tree_min_leven > new_leven:
                                tree_min_leven = new_leven
                                tree_min_lin = lin
                    if not lowest_leven:
                        lowest_leven = tree_min_leven
                        lowest_tree = tree
                        lowest_lin = tree_min_lin
                    elif tree_min_leven < lowest_leven:
                        lowest_leven = tree_min_leven
                        lowest_tree = tree
                        lowest_lin = tree_min_lin

                if lowest_lin and lowest_tree:
                    # set used lin as only element
                    self.current_task['tree'] = lowest_tree
                    self.current_task['lins'][self.app.get_ans_lang()] = [lowest_lin]

        feedback = {'correct': correct}
        # finally mark errors.
        if not feedback['correct']:
            # check words
            feedback['words'] = self.mk_word_fb(string)
            feedback['sentence'] = self.check_forms(string)

        # calculate new due date for all lexicon words in tree
        self.new_due_date(correct)
        return feedback

    def new_due_date(self, correct):
        tree_list = self.current_task['tree'].replace('(', '').replace(')', '').split(' ')
        # filter lexicon items, avoid duplicates
        lex_list = []
        for item in tree_list:
            i_split = item.split('_')
            # only look at lexicon items.
            if len(i_split) == 2 and represents_int(i_split[1]) and int(i_split[1]) not in lex_list:
                lex_list.append(int(i_split[1]))

        for w in lex_list:
            now = datetime.now()
            current_date = str_to_time(self.app.vocab.lexicon[w][Vocab.SRS][0])
            current_in_future = now < current_date

            # add to correct/false counter.  Sum of those is total num word has been seen!
            if correct:
                self.app.vocab.lexicon[w][Vocab.SRS][1] += 1
            else:
                self.app.vocab.lexicon[w][Vocab.SRS][2] += 1

            # calc a new date
            n_correct = self.app.vocab.lexicon[w][Vocab.SRS][1]
            n_false = self.app.vocab.lexicon[w][Vocab.SRS][2]
            factor = 1
            if n_correct > 0 and n_false > 0:
                factor = n_correct / n_false
            if n_correct == 0:
                factor = 1 / n_false
            elif n_false == 0:
                factor = n_correct

            add_h = factor * 3
            add_min = int((add_h % 1) * 60)
            if current_in_future:
                # if word wasn't due, add or remove half of normal amount
                add_h *= 0.5
                add_min = int((add_h % 1) * 60)
                if correct:
                    print('added ' + str(add_h) + 'h to word: #' + str(w))
                    new_date = current_date + timedelta(hours=int(add_h)) + timedelta(minutes=add_min)
                else:
                    print('removed ' + str(add_h) + 'h from word: #' + str(w))
                    new_date = current_date - (timedelta(hours=int(add_h)) + timedelta(minutes=add_min))
            else:
                print('set word #' + str(w) + 'to ' + str(add_h) + 'h from now.')
                new_date = now + timedelta(hours=int(add_h)) + timedelta(minutes=add_min)

            self.app.vocab.lexicon[w][Vocab.SRS][0] = str(new_date)
        self.app.save_user()

    def check_forms(self, answer):
        form_list = ('past_neg', 'past_pos', 'present_neg', 'present_pos', 'future_neg', 'future_pos')
        task_from = self.current_task['tree'].split(' ')[0].replace('(', '')
        ans_parse = self.app.gf.get_tree_parse(answer)

        for lang in ans_parse:
            if len(ans_parse[lang]) > 0:
                for tree in ans_parse[lang]:
                    ans_tree_list = tree.split(' ')
                    ans_form = ans_tree_list[0].replace('(', '')
                    if ans_form in form_list:
                        result = (task_from, ans_form)
                        return result

    def mk_word_fb(self, answer):
        k_lin = self.current_task['lins'][self.app.get_ans_lang()]
        expected = k_lin[0]

        # find lin that is closest to given answer
        if len(k_lin) > 1:
            for i, lin in enumerate(k_lin):
                if i == 0:
                    continue
                elif leven(expected, answer) > leven(lin, answer):
                    expected = lin

        exp_list = expected.split(' ')
        ans_list = answer.split(' ')
        fb_list = []
        fb_e_tmp = [-1] * max(len(ans_list), len(exp_list))

        # assign every a_w to an e_w
        for i, a_w in enumerate(ans_list):
            lev_idx = []
            for j, e_w in enumerate(exp_list):
                # 0: word difference, 1: distance of index, 2: idx of expected, 3: index of input
                lev_idx.append((leven(e_w, a_w), abs(i - j), j, i))
            sort_lst = sorted(lev_idx)
            merge_best_idx(fb_e_tmp, sort_lst)

        fb_idx = 0
        for k, val in enumerate(fb_e_tmp):
            if val == -1:
                # if this word has no fitting input, insert a blank
                if len(ans_list) > len(exp_list):
                    fb_list.append((ans_list[k], '', 0))
                else:
                    fb_list.append(None)
            elif fb_idx < len(ans_list):
                # insert the next word from the input list
                word = ans_list[fb_idx]
                # get the corresponding fitting word from expected list
                fit = exp_list[fb_e_tmp.index(fb_idx)]
                # calc distannce between new answer word pos and expected pos
                dist = abs(k - fb_e_tmp.index(fb_idx))
                fb_list.append((word, fit, dist))
                fb_idx += 1

        return fb_list


# split the word ID into its components
def split_id(word_id):
    word_split = word_id.split('_')
    word_idx = int(word_split[1])
    word_cat = word_split[0]
    return word_idx, word_cat


def mk_answer_dif(answer, expected):
    if len(answer) < len(expected):
        longer = expected
        shorter = answer
    else:
        longer = answer
        shorter = expected

    dif_list = [False] * len(longer)

    # find first error in prefix
    for i, c in enumerate(shorter):
        if c != longer[i]:
            break
        else:
            dif_list[i] = True

    # and now suffix
    r_longer = longer[::-1]
    r_shorter = shorter[::-1]
    r_dif = dif_list[::-1]
    for i, c in enumerate(r_shorter):
        if c != r_longer[i]:
            break
        else:
            r_dif[i] = True

    # now turn it back around
    return r_dif[::-1]


# helper for feedback stuff
def merge_best_idx(target, source):
    for tpl in source:
        e_idx = tpl[2]
        a_idx = tpl[3]
        if target[e_idx] == -1:
            target[e_idx] = a_idx
            return


# helper to check if a string is really an integer
def represents_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False
