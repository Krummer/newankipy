from distutils.core import setup

setup(
        name='ankipy',
        version='0.0.9',
        packages=[''],
        url='https://bitbucket.org/Krummer/newankipy',
        license='LGPL',
        author='Alexander Meerhoff',
        author_email='meerhoff.a@gmail.com',
        description='Language Learning with Sentence Generation',
        requires=['requests']
)
